var gulp = require('gulp');
var scsslint = require('gulp-scss-lint');
var sourcemaps = require('gulp-sourcemaps');
var uncss = require('gulp-uncss');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var csso = require('gulp-csso');
var webpack = require('gulp-webpack');



gulp.task('lint:scss',function(){
    gulp.src('src/scss/**/*.scss')
    .pipe(scsslint());
});


gulp.task('style', function() {
  return gulp.src('src/scss/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(autoprefixer({
        browsers: ['last 2 versions']
      }))
      .pipe(csso())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('.tmp'))
    .pipe(gulp.dest('dist'))
    
});




gulp.task('default', ['style'], function() {
  gulp.src([
    'src/*',
   
		'!src/scss/',
	
  ], {
    dot: true,
  })
  .pipe(gulp.dest('dist'));
});






